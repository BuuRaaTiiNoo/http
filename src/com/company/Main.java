package com.company;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        URL url = null;
        try {
            url = new URL("https://www.google.ru/?gfe_rd=cr&dcr=0&ei=LlmuWo2hE82DsAGy4abwDw&gws_rd=ssl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;
        try {
            if (url != null) {
                httpURLConnection = (HttpURLConnection) url.openConnection();
            }
        } catch (IOException e) {
            System.err.print("open connection exception");
            e.printStackTrace();
        }
        InputStream inputStream = null;
        try {
            if (url != null) {
                inputStream = url.openStream();
            }
        } catch (IOException e) {
            System.err.print("open stream exception");
            e.printStackTrace();
        }
        if (inputStream != null) {
            try (Scanner scanner = new Scanner(inputStream)) {
                String responseBody = scanner.useDelimiter("\\+").next();
                System.out.println(responseBody);
            }
        }
        if (httpURLConnection != null) {
            System.out.println(httpURLConnection.getHeaderFields());
        }
        System.out.println();
    }
}
